from dotenv import load_dotenv

load_dotenv()

from userapi.database import init_db

init_db()
