all: run

clean:
	rm -rf venv && rm -rf *.egg-info && rm -rf dist && rm -rf *.log*

venv:
	virtualenv --python=python3 venv

deps: venv
	venv/bin/pip install -e .[dev]

db: deps
	venv/bin/python init_db.py

run: db 
	FLASK_APP=userapi venv/bin/flask run

test: deps
	DATABASE_URL=sqlite:// venv/bin/python -m unittest discover -s tests
