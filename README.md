# User API

Implements a User API as per following requirements:

> A user is defined by the following attributes: email, password, first name.
>
> Build an API in charge of managing users. It should handle basic operations such as creating a user, updating full and partial user data, and retrieving one or many users based on various filters (eg.: first name, email).
>
> On user creation request, you should check if the user is located in Switzerland. Only if he is, you should allow the user creation, otherwise the request must be rejected. You can use the user IP address and https://ipapi.co/ to geolocate it.
>
>On each meaningful operation, an event must be posted to a service bus (Kafka, RabbitMQ…).
>
>As a database, we ask you to use any relational option you see fit.
>
> *Authentication and authorization are out of scope.*

## Development

Application is written using Python (>= 3.7) and Flask web framework.

It comes with a `Makefile` to support developpment operations:

Run the application:

    make run

Launch test suite:
    
    make test

This should also install all necessary libraries


## Using the API

Following snippet shows several supported operations.
[httpie](httpie.org/) is used for the example. it's allows more compact commands than curl as it natively supports JSON

```sh
# -- Creation
> http POST :5000/users name=John email=email@address.net password=lainestain
# 201 CREATED { "email": "email@address.net", "id": "85806c2495a547da9a051ada915ddf67", "name": "John" }
# *note*: password is never returned

> http POST :5000/users name=John email=email@address.net password=lainestain
# 201 CREATED { "email": "second@address.net", "id": "403c10f18c7c436a9add39d09afe0cff", "name": "Richard" }


# -- Read and Search
> http :5000/users
# 200 OK { "users": [ { "email": "email@address.net", "id": "85806c2495a547da9a051ada915ddf67", "name": "John" }, { "email": "second@address.net", "id": "403c10f18c7c436a9add39d09afe0cff", "name": "Richard" } ] }

> http :5000/users?name=John
# 200 OK { "users": [ { "email": "email@address.net", "id": "85806c2495a547da9a051ada915ddf67", "name": "John" } ] }


# -- Password are never returned, but a dedicated POST verb allows to check it:

> http POST :5000/users/check_password/85806c2495a547da9a051ada915ddf67 password=lainestain
# 200 OK

> http POST :5000/users/check_password/85806c2495a547da9a051ada915ddf67 password=nope
# 401 UNAUTHORIZED { "message": "Invalid password" }

# -- Partial update are done by adding the field to update in the POST path

> http :5000/users/85806c2495a547da9a051ada915ddf67/email value=update@email.com
# 200 OK { "email": "update@email.com", "id": "85806c2495a547da9a051ada915ddf67", "name": "John" }

> http :5000/users/85806c2495a547da9a051ada915ddf67
# 200 OK { "email": "update@email.com", "id": "85806c2495a547da9a051ada915ddf67", "name": "John" }

```

Note that creation succeed as `127.0.0.1` is whitelisted and thus bypasses the check of IP nation.

This behaviour is handled via the `POST_USER_WHITELIST_IPS` which default value is in the `.env` file.

## Notes about implementation

### User model

User is composed of 4 fields:
* email
* name
* ID
* password hash

The ID is generated using UUID v4 and stored a 16 byte blob in DB.
Some DBs such as PostgreSQL support the UUID type natively.
Compared to sequential integer IDs, they avoid the ability to discover the full user DB by simply incrementing a counter.

The password is stored as a BCrypt hash and plain form is never stored nor logged in the event bus.

### Enqueuing to event bus

Enqueuing to the event bus is done via the `app.event_logger` registered in `__init__.py`.

The event that is logged captures both the request path, method, remote address as well as the response that contains the new  state of the user.
They are created only for POST verbs.

The current implementation is limited to a file for simplicity of the API demonstration.
Setting up an actual RabbitMQ or Kafka can be done in by implementing a new `EventLogger` in `event_logger.py`.


## Notes on testing

The API was tested using an in-memory sqlite file.
Given the simplicity of the table and the issued request, SQLAlchemy can handle pretty well any major relational database.

For testing the IP address verification, 2 techniques were used:
* Allowing some IP addresses (such as the local host IP) to bypass the check as discussed above
* Mocking the request call in unittests to test the behaviour is as expected (see `test_userapi.py#92`)

Further techniques can be put in place:
* Using a VPN or a remote server in switzerland to do the actual check
* Setting up a service with a similar API as ipapi.co and targetting it instead using `COUNTRY_API_HOST` environment variable