import json
from typing import Dict, Any


class EventLogger:
    def send(self, event: Dict[str, Any]) -> None:
        ...


class EventFileLogger:
    def __init__(self, path):
        self.file = open(path, "a")

    def send(self, event):
        self.file.write(json.dumps(event))
        self.file.write("\n")
        self.file.flush()
