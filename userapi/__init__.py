import os
from flask import Flask, request
from flask_restful import Api
from flask_bcrypt import Bcrypt

from userapi.database import db_session
from userapi.resources import User, UserList, PartialUser, PasswordValidation
from userapi.event_logger import EventFileLogger

app = Flask(__name__)
app.event_logger = EventFileLogger(os.environ.get("EVENT_LOG_FILE", "event.log"))
api = Api(app, catch_all_404s=True)
bcrypt = Bcrypt(app)


@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()


@app.after_request
def log_updates(response):
    status = response.status_code
    if request.method != "GET" and status >= 200 and status < 300:
        app.event_logger.send(
            {
                "method": request.method,
                "path": request.path,
                "status": status,
                "result": response.json,
            }
        )
    return response


api.add_resource(UserList, "/users")
api.add_resource(User, "/users/<user_id>")
api.add_resource(PartialUser, "/users/<user_id>/<field>")
api.add_resource(PasswordValidation, "/users/check_password/<user_id>")
