from flask_restful import Resource, abort, reqparse, fields, marshal_with
from userapi import models
from flask import request
from http import HTTPStatus
import requests
import uuid
import os


class UuidField(fields.Raw):
    def format(self, value):
        return value.hex


UserFields = {"id": UuidField, "name": fields.String, "email": fields.String}


class IPAcceptor:
    @classmethod
    def can_create_user(cls, ip):
        return cls._is_whitlisted(ip) or cls._is_in_switzerland(ip)

    @classmethod
    def _is_whitlisted(cls, ip):
        whitelisted_ips = os.environ.get("POST_USER_WHITELIST_IPS", "").split(",")
        return ip in whitelisted_ips

    @classmethod
    def _is_in_switzerland(cls, ip):
        country_api_host = os.environ.get("COUNTRY_API_HOST", "https://ipapi.co")
        return requests.get("{}/{}/country/".format(country_api_host, ip)).text == "CH"


class UserList(Resource):
    @staticmethod
    def make_parser(required, with_password):
        parser = reqparse.RequestParser(bundle_errors=True)
        parser.add_argument("email", dest="email", required=required)
        parser.add_argument("name", dest="name", required=required)
        if with_password:
            parser.add_argument("password", dest="password", required=required)
        return parser

    POST_PARSER = make_parser.__func__(required=True, with_password=True)
    GET_PARSER = make_parser.__func__(required=False, with_password=False)

    @marshal_with(UserFields)
    def post(self):
        args = self.POST_PARSER.parse_args(strict=True)
        if not IPAcceptor.can_create_user(request.remote_addr):
            abort(
                HTTPStatus.BAD_REQUEST, message="Service only available in Switzerland"
            )
        if models.User.search(email=args["email"]):
            abort(HTTPStatus.CONFLICT, message="email address already in use")
        return models.User.create(**args), HTTPStatus.CREATED

    @marshal_with(UserFields, envelope="users")
    def get(self):
        args = self.GET_PARSER.parse_args(strict=True)
        return models.User.search(**args)


def retrieve_or_abort(user_id: str) -> models.User:
    user = models.User.get(uuid.UUID(hex=user_id))
    if user is None:
        abort(HTTPStatus.NOT_FOUND, message="No user with id {} found".format(user_id))
    return user


class User(Resource):
    @marshal_with(UserFields)
    def get(self, user_id):
        return retrieve_or_abort(user_id)


class PasswordValidation(Resource):
    def post(self, user_id):
        user = retrieve_or_abort(user_id)
        password = request.json.get("password")
        if not password:
            abort(HTTPStatus.BAD_REQUEST, message="Missing required field: password")
        if not user.check_password(password):
            abort(HTTPStatus.UNAUTHORIZED, message="Invalid password")
        return {}, HTTPStatus.OK


class PartialUser(Resource):
    @marshal_with(UserFields)
    def post(self, user_id, field):
        user = retrieve_or_abort(user_id)
        value = request.json.get("value")
        if not value:
            abort(HTTPStatus.BAD_REQUEST, message="Missing required field: value")
        if field == "email":
            user.email = value
        elif field == "name":
            user.name = value
        else:
            abort(HTTPStatus.BAD_REQUEST, message="Unknown path: {}".format(field))

        models.db_session.commit()
        return user
