from sqlalchemy import Column, Binary, String
from userapi.database import Base, db_session
from flask_bcrypt import generate_password_hash, check_password_hash
import uuid


class User(Base):
    __tablename__ = "users"
    _uid = Column(Binary(16), primary_key=True)
    name = Column(String(50))
    email = Column(String(50), unique=True)
    _pw_hash = Column(String(128))

    def __init__(self, name, email, password):
        self.id = uuid.uuid4()
        self.name = name
        self.email = email
        self.password = password

    @property
    def password(self):
        raise AttributeError("password not readable")

    @password.setter
    def password(self, password):
        self._pw_hash = generate_password_hash(password)

    @property
    def id(self):
        return uuid.UUID(bytes=self._uid)

    @id.setter
    def id(self, uuid):
        self._uid = uuid.bytes

    @classmethod
    def create(cls, **kwargs):
        user = cls(**kwargs)
        db_session.add(user)
        db_session.commit()
        return user

    @classmethod
    def search(cls, name=None, email=None):
        query = cls.query
        if name:
            query = query.filter(cls.name == name)
        if email:
            query = query.filter(cls.email == email)
        return query.all()

    @classmethod
    def get(cls, uuid):
        return cls.query.get(uuid.bytes)

    def check_password(self, password):
        return check_password_hash(self._pw_hash, password)

    def __repr__(self):
        return "<User {} name: '{}'>".format(self.id, self.name)
