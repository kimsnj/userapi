import json
import os
import unittest
import uuid

import responses
from dotenv import load_dotenv

import userapi
from userapi.models import User

load_dotenv()


class ArrayLogger:
    def __init__(self):
        self.data = []

    def send(self, event):
        self.data.append(event)


userapi.database.init_db()
userapi.app.event_logger = ArrayLogger()


class UserapiTestBase(unittest.TestCase):
    def setUp(self):
        self.events = userapi.app.event_logger.data
        self.app = userapi.app.test_client()
        self.populate_db()

    def tearDown(self):
        User.query.delete()
        self.events.clear()

    def populate_db(self):
        self.stored = [
            User.create(name="John", email="jo@hn.nn", password="12345"),
            User.create(name="Foo", email="bar@ba.z", password="6789"),
            User.create(name="Raymond", email="ray@mail.mon", password="abcdef"),
        ]

    def post(self, url, data):
        return self.app.post(
            url, data=json.dumps(data), content_type="application/json",
        )


class ListUserTestCase(UserapiTestBase):
    def test_get(self):
        """ All users should be returned
            and IDs should be set with different values
        """
        response = self.app.get("/users")
        self.assertEqual(response.status_code, 200)
        users = response.json["users"]
        self.assertEqual(3, len(users))
        ids = set()
        for user in users:
            self.assertIn("id", user)
            ids.add(user["id"])
        self.assertEqual(3, len(ids))
        self.assertNotIn("", ids)

    def test_search(self):
        response = self.app.get("/users?email=not_exsisting")
        self.assertEqual(response.status_code, 200)
        self.assertEqual([], response.json["users"])

        response = self.app.get("/users?email=bar@ba.z")
        self.assertEqual(response.status_code, 200)
        users = response.json["users"]
        self.assertEqual(1, len(users))
        self.assertEqual("Foo", users[0]["name"])
        self.assertEqual(0, len(self.events))

    def test_invalid_post(self):
        response = self.post("/users", {})
        self.assertEqual(response.status_code, 400)

        response = self.post(
            "/users", {"name": "a", "email": "b", "password": "c", "invalid_param": "e"}
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(0, len(self.events))

    def test_whitelisted_ip(self):
        os.environ["POST_USER_WHITELIST_IPS"] = "127.0.0.1"
        response = self.post("/users", {"name": "a", "email": "b", "password": "c"})
        self.assertEqual(response.status_code, 201)
        user = response.json
        self.assertNotEqual("", user["id"])
        self.assertEqual("b", user["email"])
        self.assertNotIn("password", user)
        self.assertEqual(1, len(self.events))

    @responses.activate
    def test_swiss_ip(self):
        os.environ["POST_USER_WHITELIST_IPS"] = ""
        responses.add(
            responses.GET, "https://ipapi.co/127.0.0.1/country/", body="CH", status=200
        )
        response = self.post("/users", {"name": "a", "email": "b", "password": "c"})
        self.assertEqual(response.status_code, 201)
        user = response.json
        self.assertNotEqual("", user["id"])
        self.assertEqual("b", user["email"])
        self.assertNotIn("password", user)
        self.assertEqual(1, len(self.events))

    @responses.activate
    def test_foreign_ip(self):
        os.environ["POST_USER_WHITELIST_IPS"] = ""
        responses.add(
            responses.GET, "https://ipapi.co/127.0.0.1/country/", body="ZZ", status=200
        )
        response = self.post("/users", {"name": "a", "email": "b", "password": "c"})
        self.assertEqual(response.status_code, 400)
        self.assertEqual(0, len(self.events))


class UserGetTestCase(UserapiTestBase):
    def test_no_user(self):
        response = self.app.get("/users/{}".format(uuid.uuid4().hex))
        self.assertEqual(response.status_code, 404)

    def test_get_user(self):
        response = self.app.get("/users/{}".format(self.stored[1].id.hex))
        self.assertEqual(response.status_code, 200)
        user = response.json
        self.assertEqual("Foo", user["name"])
        self.assertNotEqual(0, len(user["id"]))
        self.assertNotIn("password", user)


class PartialUserUpdateTestCase(UserapiTestBase):
    def test_no_user(self):
        response = self.post(
            "/users/{}/email".format(uuid.uuid4().hex), {"value": "new@email"}
        )
        self.assertEqual(response.status_code, 404)

    def test_invalid_format(self):
        user_id = self.stored[1].id.hex
        response = self.post("/users/{}/email".format(user_id), {})
        self.assertEqual(response.status_code, 400)

        response = self.post(
            "/users/{}/not_a_field".format(user_id), {"value": "new@email"}
        )
        self.assertEqual(response.status_code, 400)

    def test_update(self):
        user_id = self.stored[1].id.hex
        response = self.post("/users/{}/email".format(user_id), {"value": "new@email"})
        self.assertEqual(response.status_code, 200)
        user = response.json
        self.assertEqual("Foo", user["name"])
        self.assertEqual("new@email", user["email"])


class PasswordValidationTestCase(UserapiTestBase):
    def test_password_check(self):
        response = self.post(
            "/users/check_password/{}".format(self.stored[0].id.hex),
            {"password": "12345"},
        )
        self.assertEqual(response.status_code, 200)

        response = self.post(
            "/users/check_password/{}".format(self.stored[0].id.hex),
            {"password": "something_else"},
        )
        self.assertEqual(response.status_code, 401)

    def test_invalid_format(self):
        response = self.post(
            "/users/check_password/{}".format(self.stored[0].id.hex), {},
        )
        self.assertEqual(response.status_code, 400)

    def test_no_user(self):
        response = self.post(
            "/users/check_password/{}".format(uuid.uuid4().hex), {"password": "foo"},
        )
        self.assertEqual(response.status_code, 404)


if __name__ == "__main__":
    unittest.main()
