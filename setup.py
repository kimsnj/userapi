from setuptools import setup, find_packages

setup(
    name="userapi",
    version="1.0",
    long_description=__doc__,
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        "flask",
        "flask-restful",
        "SQLAlchemy",
        "flask-bcrypt",
        "requests",
        "python-dotenv",
    ],
    extras_require={"dev": ["responses"]},
)
